from flask import Flask, jsonify
import os

app = Flask(__name__)

@app.route('/')
def hello():
    message = os.environ.get('MESSAGE', 'Hello, ready to fight!')
    return jsonify({"message": message})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
